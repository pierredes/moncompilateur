// tokeniser.h : shared definition for tokeniser.l and compilateur.cpp

enum TOKEN {FEOF, UNKNOWN, NUMBER, ID, IF, THEN, ELSE, WHILE, DO, STRINGCONST, RBRACKET, LBRACKET, RPARENT, LPARENT, COMMA, 
SEMICOLON, DOT, ADDOP, MULOP, RELOP, NOT, ASSIGN};

