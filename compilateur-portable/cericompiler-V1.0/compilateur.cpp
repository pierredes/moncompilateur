//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND , POW ,WTFM};
enum TYPE {INTEGER, CHAR,DOUBLE,BOOLEAN};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, TYPE> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

		

TYPE Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	TYPE typeret=DeclaredVariables[lexer->YYText()];
	current=(TOKEN) lexer->yylex();
	return typeret;
}


TYPE Number(void){

	string n_numb=lexer->YYText();
	if(n_numb.find(".")!=-1){
		double dvar=atof(lexer->YYText());
		unsigned int *temp=(unsigned int *) &dvar;
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*temp<<", (%rsp)\t# Conversion of "<<dvar<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(temp+1)<<", 4(%rsp)\t# Conversion of "<<dvar<<" (32 bit low part)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return INTEGER;
}



TYPE Bool(void){
	
	if(strcmp(lexer->YYText(),"FALSE")==0)
		cout <<"\tpush $0\t\t# False"<<endl;

	if(strcmp(lexer->YYText(),"TRUE")==0)
		cout <<"\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;

	current=(TOKEN) lexer->yylex();
	return BOOLEAN;

}


TYPE Char(void){


	cout <<"\tmovq $0, %rax"<<endl;
	cout <<"\tmovb $'"<<lexer->YYText()<<"' , %al"<<endl;
	cout <<"\tpush %rax"<<endl;

	current=(TOKEN) lexer->yylex();
	if (current!=QUOTEMARK)
		Error(" ' était attendu");		// "'" expected
	return CHAR;
}


TYPE Expression(void);			// Called by Term() and calls Term()

TYPE Factor(void){
	TYPE type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type=Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else if (current==QUOTEMARK){
		current=(TOKEN) lexer->yylex();
		type=Char();
		current=(TOKEN) lexer->yylex();}
	else 
		if (current==NDOUBLE or current==NUMBER)
			type=Number();
	    else if(current==ID)
			type=Identifier();
		else if( (strcmp(lexer->YYText(),"TRUE")==0) or (strcmp(lexer->YYText(),"FALSE")==0))
			type=Bool();
		else
			Error("'('ou ' ou chiffre ou lettre attendue");

	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else if(strcmp(lexer->YYText(),"^")==0)
		opmul=POW;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	TYPE typef;
	TYPE typec;
	OPMUL mulop;
	typef=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		typec=Factor();
		if(typef==typec){
			cout << "\tpop %rbx"<<endl;	// get first operand
			cout << "\tpop %rax"<<endl;	// get second operand
			switch(mulop){
				case AND:
					if(typec!=BOOLEAN)
						Error("BOOLEAN attendu");
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# AND"<<endl;	// store result
					break;
				case MUL:
					if(typec!=INTEGER && typec!=DOUBLE)
						Error("valeur numérique attendu");
					if(typec==INTEGER){
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
					}
					else if (typec==DOUBLE){
						//reprendre ici
						cout<<"\tfldl	8(%rsp)\t"<<endl;
						cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
						cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
						cout<<"\tfstpl 8(%rsp)"<<endl;
						cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
					}
					break;
				case DIV:
					if(typec!=INTEGER && typec!=DOUBLE)
						Error("valeur numérique attendu");
					if(typec==INTEGER){
						cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
						cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
						cout << "\tpush %rax\t# DIV"<<endl;		// store result
					}
					else if (typec==DOUBLE){
						cout<<"\tfldl	8(%rsp)\t"<<endl;
						cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
						cout<<"\tfdivp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
						cout<<"\tfstpl 8(%rsp)"<<endl;
						cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
					}
					break;
				case MOD:
					if(typec!=INTEGER)
						Error("INTEGER attendu");
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
					cout << "\tpush %rdx\t# MOD"<<endl;		// store result
					break;
				case POW:
					if(typec!=INTEGER)
						Error("INTEGER attendu");

//					3^5
//					rbx=3	rax=5
//					rcx=rax
//					rax=rbx
//					for rcx
//						rax=rbx*rax

					cout << "\tmovq %rax, %rcx"<<endl; 	// Higher part of numerator  
					cout << "\tmovq %rbx, %rax"<<endl; 	// Higher part of numerator  


					cout << "debpow :"<<endl;
					cout << "\tsubq	$1, %rcx\t# SUB"<<endl;
					cout << "\tcmpq $0, %rcx"<<endl;
					cout << "\tje suitepow"<<endl;

					cout << "\tmulq %rbx"<<endl;	// a * b -> %rdx:%rax


					cout << "\tpush %rax\t# MUL"<<endl;	// store result
					cout << "\tpop %rax\t# MUL"<<endl;	// store result

					cout << "\tjmp debpow"<<endl;

					cout << "suitepow :"<<endl;
					cout << "\tpush %rax\t# MUL"<<endl;	// store result

					break;
				default:
					Error("opérateur multiplicatif attendu");
			}

		}
		else {
			Error("types different");}
	}

	return typef;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	TYPE typet;
	TYPE typec;
	OPADD adop;
	typet=Term(); 
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		typec=Term();
		if(typet==typec){
			cout << "\tpop %rbx"<<endl;	// get first operand
			cout << "\tpop %rax"<<endl;	// get second operand
			switch(adop){
				case OR:
					if(typec!=BOOLEAN)
						Error("BOOLEAN attendu");
					cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
					break;			
				case ADD:
					if(typec!=INTEGER&&typec!=DOUBLE)
						Error("valeur numérique attendu");
					if(typec==INTEGER)
						cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					else if(typec==DOUBLE){
						cout<<"\tfldl	8(%rsp)\t"<<endl;
						cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
						cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;

					}
					break;			
				case SUB:	
					if(typec!=INTEGER&&typec!=DOUBLE)
						Error("valeur numérique attendu");
					if(typec==INTEGER)
						cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
					else if(typec==DOUBLE){
						cout<<"\tfldl	8(%rsp)\t"<<endl;
						cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
						cout<<"\tfsubp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					}
					break;
				default:
					Error("opérateur additif inconnu");
			}
			if(typec==DOUBLE){
				cout<<"\tfstpl 8(%rsp)"<<endl;
				cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
			}
			else
				cout << "\tpush %rax"<<endl;			// store result
		}
		else {
			Error("types different");}
	}
	return typet;
}

/*
// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "\t.align 8"<<endl;
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert( std::pair<string,TYPE>(lexer->YYText(),UNSIGNED_INT) );
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert( std::pair<string,TYPE>(lexer->YYText(),UNSIGNED_INT) );
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}
*/

TYPE Type(void){
	if(current!=KEYWORD)
		Error("type attendu");
	if(strcmp(lexer->YYText(),"BOOLEAN")==0){
		current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}	
	else if(strcmp(lexer->YYText(),"INTEGER")==0){
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	else if(strcmp(lexer->YYText(),"DOUBLE")==0){
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else if(strcmp(lexer->YYText(),"CHAR")==0){
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}
	else
		Error("type inconnu");
	
}

void VarDeclaration(void){

	TYPE vartype;
	string varname;
	current=(TOKEN) lexer->yylex();
	vartype=Type();
	if(current!=ID)
		Error("Un identificater était attendu");
	varname = lexer->YYText();
	cout << "#The following line are TYPE n"<<vartype<<endl;

	if ((vartype==INTEGER) or (vartype==BOOLEAN))
		cout << varname << ":\t.quad 0"<<endl;
	else if (vartype==CHAR)
		cout << varname << ":\t.byte 0"<<endl;
	else if (vartype==DOUBLE)
		cout << varname << ":\t.double 0.0"<<endl;
	else
		Error("mauvais type");

	DeclaredVariables.insert( std::pair<string,TYPE>(varname,vartype) );
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		varname=lexer->YYText();
		if ( (vartype==INTEGER) or (vartype==BOOLEAN))
			cout << varname << ":\t.quad 0"<<endl;
		else if (vartype==CHAR)
			cout << varname << ":\t.byte 0"<<endl;
		else if (vartype==DOUBLE)
			cout << varname << ":\t.double 0.0"<<endl;
		else
			Error("mauvais type");
		DeclaredVariables.insert( std::pair<string,TYPE>(varname,vartype) );
		current=(TOKEN) lexer->yylex();
	}


}

//doit remplacer DeclarationPart
void VarDeclarationPart(void){

	if(strcmp(lexer->YYText(),"VAR")!=0)
		Error("Var attendu");
	cout << "\t.data"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t# used by printf to display 64-bit unsigned integers"<<endl;
	cout << "FormatString2:\t.string \"%c\\n\"\t# used by printf to display char"<<endl; 
	cout << "FormatString3:\t.string \"%f\\n\"\t# used by printf to display double"<<endl; 
	cout << "\t.align 8"<<endl;
	VarDeclaration();
	while(current==SEMICOLON){
		VarDeclaration();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}



// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	TYPE typese;
	TYPE typec;
	OPREL oprel;
	typese=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		typec=SimpleExpression();
		if(typese==typec){
			if (typec==DOUBLE){
				cout<<"\tfldl	(%rsp)\t"<<endl;
				cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
				cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
				cout<<"\tfcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
				cout<<"\tfaddp %st(1)\t# pop nothing"<<endl;	
			}
			else {
				cout << "\tpop %rax"<<endl;
				cout << "\tpop %rbx"<<endl;
				cout << "\tcmpq %rax, %rbx"<<endl;
			}
			switch(oprel){
				case EQU:
					cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
					break;
				case DIFF:
					cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
					break;
				case SUPE:
					cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
					break;
				case INFE:
					cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
					break;
				case INF:
					cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
					break;
				case SUP:
					cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
					break;
				default:
					Error("Opérateur de comparaison inconnu");
				typese=BOOLEAN;
			}
			cout << "\tpush $0\t\t# False"<<endl;
			cout << "\tjmp Suite"<<TagNumber<<endl;
			cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
			cout << "Suite"<<TagNumber<<":"<<endl;
			typese=BOOLEAN;
		}
		else {
			Error("types different");}
	}
	return typese;
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void){
	TYPE typevar;
	TYPE typeexp;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	typevar=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	typeexp=Expression();
	if(typevar!=typeexp){
		Error("types differents");}
	cout << "\tpop "<<variable<<endl;
	return variable;
}

void Statement(void);

void IfStatement(void){
	TYPE typeexp;
	if(strcmp(lexer->YYText(),"IF")!=0)
			Error("If attendu");
	cout << "# The following lines are a if"<<endl;
	current=(TOKEN) lexer->yylex();
	typeexp=Expression();
		if(typeexp==BOOLEAN){
			cout << "\tpop %rax"<<endl;
			cout << "\tcmpq $0,%rax"<<endl;
			cout << "\tje Else"<<endl;
			if(strcmp(lexer->YYText(),"THEN")!=0)
				Error("Then attendu");
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tjmp Suite "<< endl;
			cout << "Else :"<<endl;
			if(strcmp(lexer->YYText(),"ELSE")==0){
				current=(TOKEN) lexer->yylex();
				Statement();}
			cout << "Suite :"<<endl;}
	else {
		Error("Non booléen");}


}

void WhileStatement(void){
	TYPE typeexp;
	if(strcmp(lexer->YYText(),"WHILE")!=0)
		Error("While attendu");
	cout << "# The following lines are a while"<<endl;
	cout << "Deb :"<<endl;
	current=(TOKEN) lexer->yylex();
	typeexp=Expression();
	if(typeexp==BOOLEAN){
		cout << "\tpop %rax"<<endl;
		cout << "\tcmpq $0,%rax"<<endl;
		cout << "\tje Next"<<endl;
		if(strcmp(lexer->YYText(),"DO")!=0)
			Error("Do attendu");
		current=(TOKEN) lexer->yylex();
		Statement();
		cout << "\tjmp Deb"<<endl;
		cout << "Next :"<<endl;
	}
	else {
		Error("Non booléen");}
}

void ForStatement(void){

	if(strcmp(lexer->YYText(),"FOR")!=0)
			Error("For attendu");
	cout << "# The following lines are a for"<<endl;


	current=(TOKEN) lexer->yylex();
	string variable=AssignementStatement();
	
	if (DeclaredVariables[variable]!=INTEGER)
		Error("INTEGER attendu");

	cout << "debfor :"<<endl;

	if(strcmp(lexer->YYText(),"TO")!=0)
			Error("To attendu");
	current=(TOKEN) lexer->yylex();
	Expression();
	cout << "\tpop %rbx"<<endl;

	cout << "\taddq	$1, "<<variable<<"\t# ADD"<<endl;

	cout << "\tcmpq %rbx,"<<variable<<endl;
	cout << "\tja suitefor"<<endl;

	if(strcmp(lexer->YYText(),"DO")!=0)
			Error("Do attendu");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout << "\tjmp debfor"<<endl;

	cout << "suitefor :"<<endl;
}

void BlockStatement(void){

	if (strcmp(lexer->YYText(),"BEGIN")!=0)
		Error("Block attendu");
	current=(TOKEN) lexer->yylex();
	Statement();
	if (current== SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
}

	if(strcmp(lexer->YYText(),"END")!=0)
			Error("End attendu");

}

void Display(void){
	TYPE vardisp;
	if (strcmp(lexer->YYText(),"DISPLAY")!=0)
		Error("Block attendu");
	current=(TOKEN) lexer->yylex();
	vardisp=Expression();
	if( (vardisp==INTEGER) or (vardisp==BOOLEAN) ){
		cout << "\tpop %rdx	# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rsi\t# \"%llu\\n\""<<endl;
	   	cout << "\tmovl $1, %edi"<<endl;
		cout << "\tmovl $0, %eax"<<endl;
		cout << "\tcall __printf_chk@PLT"<<endl;
	}
	else if(vardisp==CHAR){
		cout << "\tpop %rsi	# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString2, %rdi\t# \"%c\\n\""<<endl;
	   	//cout << "\tmovl $1, %edi"<<endl;
		cout << "\tmovl $0, %eax"<<endl;
		cout << "\tcall printf@PLT"<<endl;
	}
	else if(DOUBLE){
		cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
		cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
		cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
		cout << "\tmovq $FormatString3, %rdi\t# \"%f\\n\""<<endl;
		cout << "\tmovq	$1, %rax"<<endl;
		cout << "\tcall	printf"<<endl;
		//cout << "nop"<<endl;  //WUT
		cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
	}
}

// Statement := AssignementStatement
void Statement(void){
	if(current==KEYWORD){
		if(strcmp(lexer->YYText(),"DISPLAY")==0)
			Display();
		else if(strcmp(lexer->YYText(),"IF")==0)
			IfStatement();
		else if(strcmp(lexer->YYText(),"FOR")==0)
			ForStatement();
		else if(strcmp(lexer->YYText(),"WHILE")==0)
			WhileStatement();
		else if(strcmp(lexer->YYText(),"BEGIN")==0)
			BlockStatement();
		else
			Error("mot clé inconnu");
	}
	else{
		if(current==ID)
			AssignementStatement();
		else
			Error("instruction attendue");


}
}


// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	VarDeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





