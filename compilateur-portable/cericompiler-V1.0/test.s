			# This code was produced by the CERI Compiler
	.data
FormatString1:	.string "%llu\n"	# used by printf to display 64-bit unsigned integers
FormatString2:	.string "%c\n"	# used by printf to display char
FormatString3:	.string "%f\n"	# used by printf to display double
	.align 8
#The following line are TYPE n0
a:	.quad 0
c:	.quad 0
ca:	.quad 0
xec:	.quad 0
#The following line are TYPE n3
x:	.quad 0
b:	.quad 0
z:	.quad 0
#The following line are TYPE n2
db:	.double 0.0
#The following line are TYPE n1
y:	.byte 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $8
	push $3
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai1	# If equal
	push $0		# False
	jmp Suite1
Vrai1:	push $0xFFFFFFFFFFFFFFFF		# True
Suite1:
	push $4
	push $2
	push $2
	pop %rbx
	pop %rax
	mulq	%rbx
	push %rax	# MUL
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai2	# If equal
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# OR
	push %rax
	pop z
	movq $0, %rax
	movb $'y' , %al
	push %rax
	pop y
	push $5
	push $65
	pop %rbx
	pop %rax
	movq $0, %rdx
	div %rbx
	push %rax	# DIV
	push $2
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	push $7
	push $5
	pop %rbx
	pop %rax
	movq $0, %rdx
	div %rbx
	push %rdx	# MOD
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jb Vrai3	# If below
	push $0		# False
	jmp Suite3
Vrai3:	push $0xFFFFFFFFFFFFFFFF		# True
Suite3:
	pop b
	push $21
	push $2
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop ca
	push $5
	push $3
	pop %rbx
	pop %rax
	mulq	%rbx
	push %rax	# MUL
	pop xec
	push $5
	push $3
	pop %rbx
	pop %rax
	movq %rax, %rcx
	movq %rbx, %rax
debpow :
	subq	$1, %rcx	# SUB
	cmpq $0, %rcx
	je suitepow
	mulq %rbx
	push %rax	# MUL
	pop %rax	# MUL
	jmp debpow
suitepow :
	push %rax	# MUL
	pop a
	push $0		# False
	pop x
	subq $8,%rsp			# allocate 8 bytes on stack's top
	movl	$2576980378, (%rsp)	# Conversion of 5.4 (32 bit high part)
	movl	$1075157401, 4(%rsp)	# Conversion of 5.4 (32 bit low part)
	pop db
# The following lines are a if
	push x
	push $0		# False
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai4	# If equal
	push $0		# False
	jmp Suite4
Vrai4:	push $0xFFFFFFFFFFFFFFFF		# True
Suite4:
	pop %rax
	cmpq $0,%rax
	je Else
	push a
	pop %rdx	# The value to be displayed
	movq $FormatString1, %rsi	# "%llu\n"
	movl $1, %edi
	movl $0, %eax
	call __printf_chk@PLT
	jmp Suite 
Else :
Suite :
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
