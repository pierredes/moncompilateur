			# This code was produced by the CERI Compiler
	.data
FormatString1:	.string "%llu\n"	# used by printf to display 64-bit unsigned integers
FormatString2:	.string "%c\n"	# used by printf to display char
FormatString3:	.string "%f\n"	# used by printf to display double
	.align 8
#The following line are TYPE n0
a:	.quad 0
c:	.quad 0
ca:	.quad 0
xec:	.quad 0
#The following line are TYPE n3
x:	.quad 0
b:	.quad 0
z:	.quad 0
#The following line are TYPE n2
db:	.double 0.0
#The following line are TYPE n1
y:	.byte 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $8
	push $3
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai1	# If equal
	push $0		# False
	jmp Suite1
Vrai1:	push $0xFFFFFFFFFFFFFFFF		# True
Suite1:
	push $4
	push $2
	push $2
	pop %rbx
	pop %rax
	mulq	%rbx
	push %rax	# MUL
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai2	# If equal
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# OR
	push %rax
	pop z
	movq $0, %rax
	movb $'y' , %al
	push %rax
	pop y
	push $5
	push $65
	pop %rbx
	pop %rax
	movq $0, %rdx
	div %rbx
	push %rax	# DIV
	push $2
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	push $7
	push $5
	pop %rbx
	pop %rax
	movq $0, %rdx
	div %rbx
	push %rdx	# MOD
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jb Vrai3	# If below
	push $0		# False
	jmp Suite3
Vrai3:	push $0xFFFFFFFFFFFFFFFF		# True
Suite3:
	pop b
	push $21
	push $2
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop ca
	push $5
	push $3
	pop %rbx
	pop %rax
	mulq	%rbx
	push %rax	# MUL
	pop xec
	push $5
	push $3
	pop %rbx
	pop %rax
	cmpq $0, %rbx
	je powzero4
	movq %rbx, %rcx
	movq %rax, %rbx
debpow4 :
	subq	$1, %rcx	# SUB
	cmpq $0, %rcx
	je suitepow4
	mulq %rbx
	jmp debpow4
powzero4 :
	movq $1, %rax
suitepow4 :
	push %rax	# POW
	pop a
	push $0		# False
	pop x
	subq $8,%rsp			# allocate 8 bytes on stack's top
	movl	$2576980378, (%rsp)	# Conversion of 5.4 (32 bit high part)
	movl	$1075157401, 4(%rsp)	# Conversion of 5.4 (32 bit low part)
	pop db
# The following lines are a if
	push x
	push $0		# False
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai5	# If equal
	push $0		# False
	jmp Suite5
Vrai5:	push $0xFFFFFFFFFFFFFFFF		# True
Suite5:
	pop %rax
	cmpq $0,%rax
	je Else6
	push a
	pop %rdx	# The value to be displayed
	movq $FormatString1, %rsi	# "%llu\n"
	movl $1, %edi
	movl $0, %eax
	call __printf_chk@PLT
	jmp Suite6
Else6 :
Suite6 :
	push $5
	push $0
	pop %rbx
	pop %rax
	cmpq $0, %rbx
	je powzero7
	movq %rbx, %rcx
	movq %rax, %rbx
debpow7 :
	subq	$1, %rcx	# SUB
	cmpq $0, %rcx
	je suitepow7
	mulq %rbx
	jmp debpow7
powzero7 :
	movq $1, %rax
suitepow7 :
	push %rax	# POW
	pop a
# The following lines are a if
	push x
	push $0		# False
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai8	# If equal
	push $0		# False
	jmp Suite8
Vrai8:	push $0xFFFFFFFFFFFFFFFF		# True
Suite8:
	pop %rax
	cmpq $0,%rax
	je Else9
	push a
	pop %rdx	# The value to be displayed
	movq $FormatString1, %rsi	# "%llu\n"
	movl $1, %edi
	movl $0, %eax
	call __printf_chk@PLT
	jmp Suite9
Else9 :
Suite9 :
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
